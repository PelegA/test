<?php
namespace app\controllers;

use Yii;
use yii\web\Controller;


class RbacController extends Controller
{
	/*public function actionRole()
	{
		$auth = Yii::$app->authManager;				
		
		$teammember = $auth->createRole('teammember');
		$auth->add($teammember);
		
		$teamleader = $auth->createRole('teamleader');
		$auth->add($teamleader);

		$admin = $auth->createRole('admin');
		$auth->add($admin);
	}*/

	public function actionPermissions()
	{
		$auth = Yii::$app->authManager;
		
		$indexDeals = $auth->createPermission('indexDeals');
		$indexDeals->description = 'All users can view Deals';
		$auth->add($indexDeals);
		
		$createDeal = $auth->createPermission('createDeal');
		$createDeal->description = 'Team leader and admin can create new deals';
		$auth->add($createDeal);
		
		$updateDeal = $auth->createPermission('updateDeal');
		$updateDeal->description = 'Team leader and admin can update
									deals including assignment';
		$auth->add($updateDeal);
		
		$deleteDeal = $auth->createPermission('deleteDeal');
		$deleteDeal->description = 'Team leader and admin can delete deal';
		$auth->add($deleteDeal);
		
		$viewDeal = $auth->createPermission('viewDeal');
		$viewDeal->description = 'View deals';
		$auth->add($viewDeal);

		/*$updateOwnUser = $auth->createPermission('updateOwnUser');
		$updateOwnUser->description = 'Every user can update his/her
									own profile ';
		$auth->add($updateOwnUser);
		 
		$updateOwnPassword  = $auth->createPermission('updateOwnPassword');
		$updateOwnPassword->description = 'VEvery user can update his/her
									own password';
		$auth->add($updateOwnPassword);	

		$auth = Yii::$app->authManager;
		
		$createUser = $auth->createPermission('createUser');
		$createUser->description = 'Admin can create new users';
		$auth->add($createUser);
		
		$updateUser = $auth->createPermission('updateUser');
		$updateUser->description = 'Admin can update all users';
		$auth->add($updateUser);

		

		$updatePassword = $auth->createPermission('updatePassword');
		$updatePassword->description = 'Admin can update password for 
									all users';
		$auth->add($updatePassword);*/
	}

	public function actionChilds()
	{
		$auth = Yii::$app->authManager;				
		//team member
		$teammember = $auth->getRole('teammember');

		$indexDeals = $auth->getPermission('indexDeals');
		$auth->addChild($teammember, $indexDeals);

		$viewDeal = $auth->getPermission('viewDeal');
		$auth->addChild($teammember, $viewDeal);
		//team leader can view and more
		$teamleader = $auth->getRole('teamleader');
		$auth->addChild($teamleader, $teammember);
		
		$createDeal = $auth->getPermission('createDeal');
		$auth->addChild($teamleader, $createDeal);

		$updateDeal = $auth->getPermission('updateDeal');
		$auth->addChild($teamleader, $updateDeal);
		
		$deleteDeal = $auth->getPermission('deleteDeal');
		$auth->addChild($teamleader, $deleteDeal);
		//admin can CRUD too, same permissions like team leader
		$admin = $auth->getRole('admin');
		$auth->addChild($admin, $teamleader);		
		
	}
}