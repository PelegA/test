<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "deal".
 *
 * @property integer $id
 * @property integer $leadId
 * @property string $name
 * @property integer $amount
 */
class Deal extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'deal';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['leadId', 'name', 'amount'], 'required'],
            [['leadId', 'amount'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
	public function getLeadItem()
    {
        return $this->hasOne(Lead::className(), ['id' => 'name']);
    } 
	public static function getDeals()
	{
		$allDeals = self::find()->all();
		$allDealsArray = ArrayHelper::
					map($allDeals, 'id', 'name');
		return $allDealsArray;						
	}
	public static function getDealsWithAllDeals()
	{
		$allDeals = self::getDeals();
		$allDeals[-1] = 'All Deals';
		$allDeals = array_reverse ( $allDeals, true );
		return $allDeals;
	}
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'leadId' => 'Lead ID',
            'name' => 'Name',
            'amount' => 'Amount',
        ];
    }
}
