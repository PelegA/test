<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\DealSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Deals';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="deal-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Deal', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'leadId',
			/*[
				'attribute' => 'leadId',
				'label' => 'Lead Id',
				'format' => 'raw',
				'value' => function($model){
					return $model->leadItem->name;
				},
				'filter'=>Html::dropDownList('DealSearch[lead]', $lead, $leads, ['class'=>'form-control']),
			],*/
            'name',
            'amount',
			/*[
				'attribute' => 'amount',
				'label' => 'Amount',
				'format' => 'raw',
				'value' => $amount = Deal::find()->where(['amount' => $this->amount])->all();
				},
				'filter'=>Html::dropDownList('LeadSearch[owner]', $owner, $owners, ['class'=>'form-control']),
			],*/

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
